# Full System Setup (endeavourOS)

## Initial Setup

- Activate maximize and minimize button ins gnome-tweaks - window titlebars

- install gnome extensions: caffeine, dash to dock, tray icons: reloaded, user themes
  - optional: vitals
  - settings:
    - dash to dock: position on screen - left, show trash icon - off, show overview on startup - off

- Set win+enter 'gnome-terminal' shortcut

- Set Ctrl + win + enter as `albert toggle` shortcut

- Use switch window instead of switch application shortcut for alt-tab

- Set solarized dark theme in terminal and solarized color palette

- Set solarized theme in gradience

- remove mouse acceleration setting

- [SSH keys for GitLab and GitHub](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)

- Install [sublime text](https://www.sublimetext.com/docs/3/linux_repositories.html) and [sublime merge](https://www.sublimemerge.com/docs/linux_repositories)
  
  - See sublime directory for configs and packages
  - Maybe fix dualboot time: `timedatectl set-local-rtc 1 --adjust-system-clock`
    
## Pacman Packages

- base-devel

- powerline-fonts

- pandoc

- thunderbird

- python-pipenv

- vlc

- julia

- okular

- texlive-meta

- r

- gcc-fortran

- clang

- cmake

- texstudio

- discord

- ruby

- tk

- docker

- signal-desktop

- gsl

- telegram-desktop

- bitwarden

- lapacke

- openmp

- Obsidian

- gnome-terminal

- gnome-system-monitor

- gnome-browser-connector
  
  or `sudo pacman -S base-devel powerline-fonts pandoc thunderbird python-pipenv vlc julia okular texlive-meta r gcc-fortran clang cmake texstudio discord ruby tk docker signal-desktop gsl telegram-desktop bitwarden lapacke openmp obsidian gnome-terminal gnome-system-monitor gnome-browser-connector`

## AUR packages

- zotero-bin
  
  - Install Better BibTex for Zotero Addon

- pycharm-professional (or community from pacman)

- rstudio-desktop-bin

- spotify

- zoom

- slack-desktop

- google-chrome

- keybase-gui

- foxitreader

- pomatez

- whatsapp-nativefier

- albert

- radian

- gradience

- adw-gtk-theme
  
or `yay -S zotero-bin rstudio-desktop-bin spotify zoom slack-desktop google-chrome keybase-gui foxitreader pomatez whatsapp-nativefier pycharm-professional albert radian gradience adw-gtk-theme`


## RStudio

### .R/Makevars

See: https://discourse.mc-stan.org/t/speedup-by-using-external-blas-lapack-with-cmdstan-and-cmdstanr-py/25441 for some discussion of makevars for cmdstan

CXX14FLAGS=-O3 -march=native -mtune=native -fPIC -mmmx -msse -msse2 -msse3 -mssse3 -msse4.1 -msse4.2

CXX=clang++
CC=clang
MAKE=make -j6

```{r}
install.packages("devtools")
Sys.setenv(DOWNLOAD_STATIC_LIBV8 = 1)
install.packages("V8")
install.packages(c("tidyverse", "coda","mvtnorm", "dagitty", "extraDistr", "stringi", "ggdag", "gridExtra", "ggthemes", "tidybayes", "skimr", "bookdown", "GGally", "languageserver", "stringi", "knitr", "kableExtra", "patchwork", "latex2exp", "lintr", "roxygen2", "bayesboot", "caret", "httpgd", "Rfast", "doRNG", "viridis", "blogdown", "rmutil"), repos="https://cloud.r-project.org/",dependencies=TRUE)
library(devtools)
library(remotes)
install_github("stan-dev/cmdstanr")
install_github("paul-buerkner/brms")
install_github("rmcelreath/rethinking")
install_github("stan-dev/posterior")
install_github("stan-dev/loo")
install_github("stan-dev/projpred")
install_github("stan-dev/bayesplot")
install_github("sims1253/bayesfam")
install_github("sims1253/bayeshear")
install_github("sims1253/meadow")
install_github("sims1253/bayesim")
library(cmdstanr)
cpp_options = list("CXX=clang++", "CC=clang", "CXXFLAGS += -march=native -mtune=native -DEIGEN_USE_BLAS -DEIGEN_USE_LAPACKE", "LDLIBS += -lblas -llapack -llapacke", "STAN_THREADS = TRUE", "STAN_CPP_OPTIMS = TRUE")
cmdstanr::cmdstan_make_local(cpp_options = cpp_options, append = FALSE)
install_cmdstan(cores = 6, overwrite = TRUE)
```


# Archive

## Antergos or now Manjaro

- Set pacman colors in /etc/pacman.conf

- Set win+enter 'gnome-terminal' shortcut

- Set Ctrl + win + enter as `albert toggle` shortcut

- Use switch window instead of switch application shortcut for alt-tab

- Set solarized dark theme in terminal and solarized color palette

- Set flat mouse profile in gnome-tweaks

- In layouts -> settings turn off "Firefox Gnome Theme"

- In Gradience use the solarized light preset

- Install [yay](https://github.com/Jguer/yay)

- [SSH keys for GitLab and GitHub](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)

- Install [sublime text](https://www.sublimetext.com/docs/3/linux_repositories.html) and [sublime merge](https://www.sublimemerge.com/docs/linux_repositories)
  
  - See sublime directory for configs and packages
  - Maybe fix dualboot time: `timedatectl set-local-rtc 1 --adjust-system-clock`

- Set tlp setting STOP_CHARGE_THRESH_BAT0 to 80
  
**(not necessary with current manjaro)**

- install [oh-my-zsh](https://ohmyz.sh/) 
  
  - .zshrc set agnoster theme
  - .oh-my-zsh/themes/agnoster... set solarized dark and remove context
  - 

- Add `zsh-syntax-highlighting zsh-autosuggestions zsh-history-substring-search` to .zshrc plugins and run 
  
  - `git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`  
  
  - `git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`
  
  - `git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search`
    
### Pacman Packages

- base-devel

- powerline-fonts

- pandoc

- thunderbird

- the_silver_searcher (windows needs a [port](https://github.com/JFLarvoire/the_silver_searcher))

- python-pipenv

- pycharm-community-edition (or professional from AUR)

- vlc

- julia

- okular

- texlive-meta

- r

- gcc-fortran

- clang

- cmake

- texstudio

- discord

- tlp

- ruby

- tk

- docker

- docker-compose

- signal-desktop

- gsl

- telegram-desktop

- bitwarden

- lapacke

- openmp

- Obsidian
  
  or `sudo pacman -S base-devel powerline-fonts pandoc thunderbird the_silver_searcher python-pipenv pycharm-community-edition vlc julia okular texlive-most r gcc-fortran clang cmake texstudio discord tlp ruby tk docker docker-compose signal-desktop gsl telegram-desktop bitwarden lapacke openmp obsidian`

### AUR packages

- zotero
  
  - Install Better BibTex for Zotero Addon

- pycharm-professional (or community from pacman)

- rstudio-desktop

- v8-r (yay -G v8-r, makepkg -si)

- spotify

- gnome-shell-pomodoro

- zoom

- slack-desktop

- google-chrome

- keybase-gui

- marktext-bin

- foxitreader

- pomatez

- whatsapp-nativefier

- albert

- radian

  
  or `yay -S zotero rstudio-desktop spotify zoom slack-desktop google-chrome keybase-gui marktext-bin foxitreader pomatez whatsapp-nativefier pycharm-professional albert radian`
