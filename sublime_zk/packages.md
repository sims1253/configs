Installed Packages:
- HighlightWords
- Markdown Extended
- MarkdownPreview
- Package Control
- PlainNotes
- PlainTasks
- SideBarEnhancements
- TrailingSpace
- LateXTools
- Pretty JSON
- GitGutter
- All Autocomplete
- LSP
- R-IDE
- Citer
- SendCode
- Terminus
- BracketHighlighter
- WordCount


External Packages:
- [sublime_zk](https://github.com/renerocksai/sublime_zk)

Install German dictionaries:
- https://github.com/titoBouzout/Dictionaries
